import { LitElement, html, css } from 'lit-element';

class DatosBEcario  extends LitElement {

  static get styles() {
    return css`
    :host {
      display: block;
    }
    .divContainer{
      padding: 1rem 8rem;
    }
    .label{
      background: transparent;
      border: none;
      border-bottom: 1px solid #83A4C5;
      width: 2rem;
      outline: none;
      font-style: italic;
      font-size: large;
      padding: 1px 4px 1px;
    }
    .nameC{
      width: 13rem;
    }
    .tipoBeca{
      width: 6rem;
    }
    .statusBeca{
      width: 6rem;
    }
    .fixDiv{
      float: left;
    }
    .marginDiv{
      margin-left: 5rem;
    }
    .floatNone{
      float: none;
    }
    .form{
      margin-left:15%;
    }
    `;
  }

  static get properties() {
    return {
      resultado: {type:Object },
        becario: { type:Object}
    };
  }

  constructor() {
    super();
    var entrada = window.localStorage.getItem("funBecario");
    var jwt = window.localStorage.getItem("jwt");
    console.log("constructor: "+ entrada);
    this.cargarDatos("http://localhost:8091/api/becarios/",entrada,jwt);
    this.becario = {
        "funBecario": "0",
        "nombre": "",
        "edad": 0,
        "correo": "",
        "tipo_de_beca": "",
        "tutor": "",
        "escuela": "",
        "resumenPago": {
            "totMeses": 0,
            "mesesPagados": 0,
            "mesActual": 0,
            "monto": 0
        },
        "status": ""
    };
  }

  render() {
    return html`
      <div class="divContainer" >
        <div style="margin-left:42%">
            <h4>Datos Personales</h4>
        </div>
        <div class="form">
          <div class="fixDiv marginDiv">
              <label for="ifun">FUN:</label>
              <input type="number" id="ifun" class="label" .value="${this.becario.funBecario}" disabled></input>
          </div>
          <div class="fixDiv marginDiv">    
              <label for="inombre">Nombre:</label>
              <input type="text" id="inombre" class="label nameC" .value="${this.becario.nombre}" disabled></input>
          </div>
          <div class="fixDiv marginDiv">
              <label for="iedad">Edad:</label>
              <input type="text" id="iedad" class="label" .value="${this.becario.edad}" disabled></input>
          </div>
          <div class="fixDiv marginDiv">
              <label for="itutor">Tutor:</label>
              <input type="text" id="itutor" class="label nameC" .value="${this.becario.tutor}" disabled></input>
          </div>
        </div>
        <div class="form" style="clear:both; padding-top:2rem">
            <div class="fixDiv marginDiv">
                <label for="iescuela">Escuela:</label>
                <input type="text" id="iescuela" class="label nameC" .value="${this.becario.escuela}" disabled></input>
            </div>
            <div class="fixDiv marginDiv">
                <label for="itbeca">Tipo de Beca:</label>
                <input type="text" id="itbeca" class="label tipoBeca" .value="${this.becario.tipo_de_beca}" disabled></input>
            </div>
            <div class="fixDiv marginDiv">
                <label for="iestatus">Estado de Beca:</label>
                <input type="text" id="iestatus" class="label statusBeca" .value="${this.becario.status}" disabled></input>
            </div>
        </div>
        <div style="margin-left:42%; margin-top: 5%; clear:both;">
          <h4>Resumen de Pago</h4>
        </div>
        <div class="form" style="margin-left:17%">
          <div class="fixDiv marginDiv">
            <label for="itotMeses">Total de Meses:</label>
            <input type="text" id="itotMeses" class="label " .value="${this.becario.resumenPago.totMeses}" disabled></input>
          </div>
          <div class="fixDiv marginDiv">    
            <label for="imesesPagados">Meses Pagados:</label>
            <input type="text" id="imesesPagados" class="label " .value="${this.becario.resumenPago.mesesPagados}" disabled></input>
          </div>
          <div class="fixDiv marginDiv">
            <label for="imesActual">Mes Actual:</label>
            <input type="text" id="imesActual" class="label " .value="${this.becario.resumenPago.mesActual}" disabled></input>
          </div>
          <div class="fixDiv marginDiv">
            <label for="imonto">Monto Mensual:</label>
            <input type="text" id="imonto" class="label" style="width:3rem" .value="${this.becario.resumenPago.monto}" disabled></input>
          </div>
        </div>
        <div style="margin-left:43%; margin-top: 5%; clear:both;">
          <button @click="${this.obtenerDispersion}">Obtener Pago</button>
          <button @click="${this.obtenerHistorial}">Historial Retiro</button>
        </div>
      </div>
    `;
  };

  cargarDatos(url,entrada,jwt){
    console.log(url+entrada);

    const options = {
      method: 'GET',
      headers: {'Content-type': 'application/json',
                'Authorization': jwt}
     }
    console.log(url+entrada);
    console.log(options);

   fetch(url+entrada,options)
    .then(response => {
        console.log(response);
        if(!response.ok){
            throw response;
        }
        return response.json();
    })
    .then(data => {
        console.log(data);
        this.becario = data;
    })
    .catch(error => {
        alert("Problema en Fetch" + error);
    })
  };

  obtenerDispersion(){
    window.location = "/web/retiro.html";
  }

  obtenerHistorial(){
    window.location = "/web/historial.html";
  }

}

customElements.define('datos-becario', DatosBEcario);