import { LitElement, html, css } from 'lit-element';

class DatosRetiroHistorial  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .divContainer{
        padding: 1rem 8rem;
      }
      .label{
        background: transparent;
        border: none;
        border-bottom: 1px solid #83A4C5;
        width: 7rem;
        outline: none;
        font-style: italic;
        font-size: large;
        padding: 1px 4px 1px;
        margin: 8px;
      }
      .nameC{
        width: 13rem;
      }
      .tipoBeca{
        width: 6rem;
      }
      .statusBeca{
        width: 6rem;
      }
      .fixDiv{
        float: left;
      }
      .marginDiv{
        margin-left: 5rem;
      }
      .floatNone{
        float: none;
      }
      .form{
        margin-left:15%;
      }
      .fontcl{
        font-family: sans-serif;
      }
    `;
  }

  static get properties() {
    return {
        historial: {type: Array}
    };
  }

  constructor() {
    super();
    var entrada = window.localStorage.getItem("funBecario");
    var jwt = window.localStorage.getItem("jwt"); 
    this.cargarDatos("http://localhost:8091/api/becarios/", entrada + "/retiro",jwt);
  }

  render() {
    return html`
      <div class="divContainer">
        <h3 class="fontcl">Historial Becario FUN: ${this.historial[0].funBecario}</h3>
        <button @click="${this.obtenerDispersion}">Regresar</button>
        ${this.historial.map(retiro => 
            html `
                <div class="form">
                    <!--<label for="ifun">FUN:</label>
                    <input type="number" id="ifun" .value="${retiro.funBecario}" disabled></input>
                    <br/>--!>
                    <label class="fontcl" for="iCodigo">Codigo:</label>
                    <br/>
                    <input class="label" type="text" id="iCodigo" .value="${retiro.codigo.substring(0,4)}" disabled></input>
                    <input class="label" type="text" id="iCodigo" .value="${retiro.codigo.substring(5,9)}" disabled></input>
                    <input class="label" type="text" id="iCodigo" .value="${retiro.codigo.substring(10,13)}" disabled></input>
                    <br/>
                    <label class="fontcl" for="iClave_seguridad">Clave Seguridad:</label>
                    <br/>
                    <input class="label" type="text" id="iClaveF" .value="${retiro.claveSeguridad}" disabled></input>
                    <br/>
                    <label class="fontcl" for="iMonto">Monto:</label>
                    <br/>
                    <input class="label" type="text" id="iMonto" .value="${retiro.monto}" disabled></input>
                    <br/>
                    <label class="fontcl" for="iFecha_exp">Fecha de expedicion:</label>
                    <br/>
                    <input class="label" type="text" id="iFecha_exp" .value="${retiro.fechaExpedicion}" disabled></input>
                    <br/>
                    <label class="fontcl" for="iFecha_ven">Fecha de vencimiento:</label>
                    <br/>
                    <input class="label" type="text" id="iFecha_ven" .value="${retiro.fecha_vencimiento}" disabled></input>
                </div>
                <hr/>
            `)}
      </div>
    `;
  }

  cargarDatos(url, entrada, jwt){
    const options = {
        method: 'GET',
        headers: {'Content-type': 'application/json',
                  'Authorization': jwt}
       }
      console.log(url+entrada);
      console.log(options);
  
     fetch(url+entrada,options)
      .then(response => {
          console.log(response);
          if(!response.ok){
              throw response;
          }
          return response.json();
      })
      .then(data => {
          this.historial = data;
          console.log(data);
      })
      .catch(error => {
          alert("Problemas con el fetch retiro: " + error);
      })
    }

    obtenerDispersion(){
      window.location = "/web/becario.html";
    }
  
}


customElements.define('datos-retiro-historial', DatosRetiroHistorial);