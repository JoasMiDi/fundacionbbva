import { LitElement, html, css } from 'lit-element';

class LoginFundacion  extends LitElement {

  static get styles() {
    return css`
      button,input,optgroup,select,textarea{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,input{overflow:visible}
      input{margin-bottom:1rem; border-radius:.25rem}
      button{margin-bottom:1rem; border-radius:.25rem}
       
      .contenedor {
        display: flex;
        align-items: center;
        justify-content: center;
        
        margin: 0;
        padding: 0;
        min-width: 100vw;
        min-height: 100vh;
        width: 100%;
        height: 100%;
      }

      .central {
        max-width: 320px;
        width: 100%;
      }

      
      .hijo{
          padding: 70px 0;
          text-align: center;
          border-radius: 6px;
          background: white;
          height: 116px;
      }
       
      .login {
        width: 100%;
        padding: 50px 36px;
        background-color: white;
        
        -webkit-box-shadow: 0px 0px 5px 5px rgba(0,0,0,0.15);
        -moz-box-shadow: 0px 0px 5px 5px rgba(0,0,0,0.15);
        box-shadow: 0px 0px 5px 5px rgba(0,0,0,0.15);
        
        border-radius: 3px 3px 3px 3px;
        -moz-border-radius: 3px 3px 3px 3px;
        -webkit-border-radius: 3px 3px 3px 3px;
      }
    .titulo {
      font-size: 135%;
      color: rgb(61 67 70);
        padding-left: 20px;
      font-weight: bold;
      }

    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
      }
      
      /* Modal Content */
      .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 35%;
      }
      
      /* The Close Button */
      .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
      }
      
      .close:hover,
      .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
      }
    `;
  }

  static get properties() {
    return {
        user: {type:String},
        password : {type:String},
        modalview: {type:String},
        modalcontent: {type:String}
    };

  }

  constructor() {
    super();
    this.user = "";
    this.password = "";
    this.modalview = "none",
    this.modalcontent = "";
  }

  render() {
    return html`
    <div class="contenedor" >
       <div class="central">
          <div class="login">
            <img src="/src/img/bbva.jpg" width="200"
            height="100">
            </br>
            
            <div> 
            <input type="text" class="form-group" id="iusers"  .value="${this.user}" @input="${this.updateUser}" placeholder="Usuario" required></div>
            <div> 
            <input type="password" id="ipassword" .value="${this.password}" @input="${this.updatePassword}" placeholder="Password" required>
            </div>
            <button class="button1" @click="${this.login}">Acceder</button>
           </div>
           </div>
           </div>
     
     
          <div id="myModal" class="modal" style="display: ${this.modalview}">
    
          <!-- Modal content -->
          <div class="modal-content">
            <span class="close"  @click="${this.hide}"" >&times;</span>
            <p>${this.modalcontent}</p>
          </div>

    `;
  }

  hide(){
    this.modalview = "none";
  }

  login(){
    var banderau= true;
    var banderap= true;
    if(this.user===""||this.user===null){
        banderau = false;
    }
    if(this.password===""||this.password===null){
        banderap = false;
    }

    if(banderau===true && banderap===true){
      var body =  {
        "usuario": this.user,
        "contraseña": this.password
    }

    const options = {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {'Content-type': 'application/json'}
    }

      fetch("http://localhost:8091/api/login/",options).then(response =>{
        if(!response.ok){
          throw response; 
        }else if(response.status===400){
          throw response;
        }else if(response.status===200){
          console.log(response.status);
        }
        return response.json();
      }).then (data =>{
        if(data.jwt != null && data.jwt !=""){
          if(data.location != null && data.location !=""){
            console.log(data.jwt);
            console.log(data.location);
            console.log(data.location.substring(data.location.length, data.location.length - 1));
            window.localStorage.setItem("funBecario", data.location.substring(data.location.length, data.location.length - 1));
            window.localStorage.setItem("jwt", data.jwt);
            window.location = "/web/becario.html"
          }else{
            // alert ("Problema con el data location" );
            this.modalview = "initial";
            this.modalcontent = "Problema con la autentificación ";
          }
        }else{
          // alert ("Problema con el data jwt" );
          this.modalview = "initial";
          this.modalcontent = "Problema con el data jwt ";
        }
      }).catch (error =>{
          // alert ("Problema con la autentificación"+ error );
          this.modalview = "initial";
          this.modalcontent = "Problema con la autentificación" + error;
      })
    }else{
        // alert("No has llenado los campos")
        this.modalview = "initial";
        this.modalcontent = "Debes de llenar todos los campos";
    }

   }


 updateUser(e) {
    this.user = e.target.value;
 }
 updatePassword(e) {
    this.password = e.target.value;
 }

  
}




customElements.define('login-fundacion', LoginFundacion);

