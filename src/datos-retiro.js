import { LitElement, html, css } from 'lit-element';

class DatosRetiro  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .divContainer{
        padding: 1rem 8rem;
      }
      .label{
        background: transparent;
        border: none;
        border-bottom: 1px solid #83A4C5;
        width: 4rem;
        outline: none;
        font-style: italic;
        font-size: large;
        padding: 1px 4px 1px;
      }
      .nameC{
        width: 7rem;
      }
      .tipoBeca{
        width: 6rem;
      }
      .statusBeca{
        width: 6rem;
      }
      .fixDiv{
        float: left;
      }
      .marginDiv{
        margin-left: 5rem;
      }
      .floatNone{
        float: none;
      }
      .form{
        margin-left:15%;
      }
      .centerText{
        text-align: center
      }
    `;
  }

  static get properties() {
    return {
        retiro: {type: Object}
    };
  }

  constructor() {
    super();
    var entrada = window.localStorage.getItem("funBecario");
    var jwt = window.localStorage.getItem("jwt");
    console.log(localStorage)
    console.log(jwt);
    console.log("constructor: "+ entrada);
    this.cargarDatos("http://localhost:8091/api/retiros/", entrada,jwt);
    this.retiro = {
      "id": 0,
      "funBecario": "1",
      "codigo": "1111-1111-1111",
      "claveSeguridad": "1111",
      "monto": 15000,
      "fechaExpedicion": "2021-16-15",
      "fecha_vencimiento": "2021-16-15",
      "status": ""
    }
  }

  render() {
    return html`
      <div class="divContainer" >
        <div style="margin-left:41.5%">
            <h3>Retiro Sin Tarjeta</h3>
        </div>
        <div class="form" style="clear:both; font-size:small">
          <div>
            <div class="fixDiv" style="margin-left:31.5%">    
                <input type="text" id="inombre" class="label nameC centerText" style="border:none;font-size:inherit;" .value="${this.retiro.fechaExpedicion}" ></input>
            </div>
          </div>
        </div>
        

        <div class="form" style="clear:both; margin-top: 5rem">
          <div style="margin-left:34%; margin-top:"3%">
              <h4>Código</h4>
          </div>
          <div style="margin-left:21.5%">
            <div class="fixDiv marginDiv">    
                <input type="text" id="inombre" class="label centerText" .value="${this.retiro.codigo.substring(0,4)}" disabled></input>
            </div>
            <div class="fixDiv marginDiv">
                <input type="text" id="iedad" class="label centerText" .value="${this.retiro.codigo.substring(5,9)}" disabled></input>
            </div>
            <div class="fixDiv marginDiv">
                <input type="text" id="itutor" class="label centerText" .value="${this.retiro.codigo.substring(10,13)}" disabled></input>
            </div>
          </div>
        </div>
        <div class="form" style="clear:both; margin-top: 5rem">
          <div style="margin-left:32.5%; margin-top:"3%">
              <h4>Clave Seguridad</h4>
          </div>
          <div style="margin-left:29.5%">
            <div class="fixDiv marginDiv">    
                <input type="text" id="inombre" class="label centerText" .value="${this.retiro.claveSeguridad}" disabled></input>
            </div>
        </div>
        <div class="form" style="clear:both; margin-top: 5rem">
          <div style="margin-left:23%; margin-top:"3%">
              <h4>Monto</h4>
          </div>
          <div style="margin-left:17.5%">
            <div class="fixDiv marginDiv">    
                <input type="text" id="inombre" class="label centerText" .value="${this.retiro.monto}" disabled></input>
            </div>
        </div>
        <div class="form" style="clear:both; margin-top: 5rem">
          <div style="margin-left:6.5%; margin-top:"3%">
              <h4>Fecha Vencimiento</h4>
          </div>
          <div style="margin-left:1.5%">
            <div class="fixDiv marginDiv">    
                <input type="text" id="inombre" class="label nameC centerText" style="border:none;font-size:inherit" .value="${this.retiro.fecha_vencimiento}" ></input>
            </div>
          </div>
        </div>
        <div style="margin-left:22.5%; margin-top: 5%; clear:both;">
          <button @click="${this.obtenerDispersion}">Regresar</button>
        </div>
      </div>
    `;
  }
  cargarDatos(url, entrada, jwt){
    const options = {
      method: 'GET',
      headers: {'Content-type': 'application/json',
                'Authorization': jwt}
     }
    console.log(url+entrada);
    console.log(options);

   fetch(url+entrada,options)
    .then(response => {
        console.log(response);
        if(!response.ok){
            throw response;
        }
        return response.json();
    })
    .then(data => {
        this.retiro = data;
        console.log(data);
    })
    .catch(error => {
        alert("Problemas con el fetch retiro: " + error);
    })
  }

  obtenerDispersion(){
    window.location = "/web/becario.html";
  }
}

customElements.define('datos-retiro', DatosRetiro);